<?php
/**
 * @widget Sociale media links from TrendPress contact info
 */
class rby_social extends WP_Widget {
	public function __construct()  {
		parent::__construct('rby_social', __('Social media links','rby'), 'description='.__('Shows links to specified social network profiles','rby'));
	}
	
	function form($instance) {
		$title = esc_attr($instance['title']);
		$type = esc_attr($instance['type']);
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">
				<strong><?php _e('Title'); ?></strong><br />
				<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
			</label>
		</p>
		<p>
			<label>
				<strong><?php _e('Icon types','rby'); ?></strong><br />
				<select class="widefat" name="<?php echo $this->get_field_name('type'); ?>">
					<option <?php if($type == 'large-icons') echo 'selected="selected"'; ?> value="large-icons"><?php _e('Large icons','rby'); ?></option>
					<option <?php if($type == 'small-icons') echo 'selected="selected"'; ?> value="small-icons"><?php _e('Small icons','rby'); ?></option>
					<option <?php if($type == 'small-icons-text') echo 'selected="selected"'; ?> value="small-icons-text"><?php _e('Small icons with text','rby'); ?></option>
				</select>
			</label>
		</p>
		<p><?php printf(__('Change the contents of this widget on the <a href="%1$s">contact information</a> page.', 'rby'), admin_url('options-general.php?page=rby-information')); ?></p>
		<?php
	}
	
	function update($new_instance,$old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['type'] = $new_instance['type'];
				
		return $instance;
	}
	
	function widget($args,$instance) {		
		$title = apply_filters('widget_title', $instance['title']);
		$type = $instance['type'];
		extract($args);
	?>
		<?php echo $before_widget; ?>
			<?php if ($title) { echo $before_title . $title . $after_title; } ?>
			<ul class="social <?php echo $type; ?> clearfix">
				<?php if($twitter = get_option('rby-twitter')) { ?>
					<li class="twitter">
						<a rel="external" href="<?php echo $twitter; ?>" title="<?php _e('Follow us on Twitter','rby') ?>">
							<i class="ion-social-twitter"></i><span class="title"><?php _e('Follow us on Twitter','rby') ?></span>
						</a>
					</li>
				<?php } ?>
				<?php if($facebook = get_option('rby-facebook')) { ?>
					<li class="facebook">
						<a rel="external" href="<?php echo $facebook; ?>" title="<?php _e('Like our Facebook page','rby') ?>">
							<i class="ion-social-facebook"></i><span class="title"><?php _e('Like our Facebook page','rby') ?></span>
						</a>
					</li>
				<?php } if($instagram = get_option('rby-instagram')) { ?>
					<li class="instagram">
						<a rel="external" href="<?php echo $instagram; ?>" title="<?php _e('Follow us on Instagram','rby') ?>">
							<i class="ion-social-instagram"></i><span class="title"><?php _e('Follow us on Instagram','rby') ?></span>
						</a>
					</li>
				<?php } if($linkedin = get_option('rby-linkedin')) { ?>
					<li class="linkedin">
						<a rel="external" href="<?php echo $linkedin; ?>" title="<?php _e('Connect with us on LinkedIn','rby') ?>">
							<i class="ion-social-linkedin"></i><span class="title"><?php _e('Connect with us on LinkedIn','rby') ?></span>
						</a>
					</li>
					<?php } if($googleplus = get_option('rby-googleplus')) { ?>
					<li class="googleplus">
						<a rel="external" href="<?php echo $googleplus; ?>" title="<?php _e('Add us on Google+','rby') ?>">
							<i class="ion-social-googleplus"></i><span class="title"><?php _e('Add us on Google+','rby') ?></span>
						</a>
					</li>
				<?php } if($youtube = get_option('rby-youtube')) { ?>
					<li class="youtube">
						<a rel="external" href="<?php echo $youtube; ?>" title="<?php _e('View our YouTube channel','rby') ?>">
							<i class="ion-social-youtube"></i><span class="title"><?php _e('View our YouTube channel','rby') ?></span>
						</a>
					</li>
				<?php } if($newsletter = get_option('rby-newsletter')) { ?>
					<li class="email">
						<a href="<?php echo $newsletter; ?>" title="<?php _e('E-mail newsletter','rby'); ?>">
							<i class="ion-ios-email"></i><span class="title"><?php _e('E-mail newsletter','rby'); ?></span>
						</a>
					</li>
				<?php } ?>
				<?php if(get_option('rby-rss') == 'true') { ?>
					<li class="rss">
						<a href="<?php bloginfo('rss2_url'); ?>" title="<?php _e('Subscribe via RSS','rby') ?>">
							<i class="ion-social-rss"></i><span class="title"><?php _e('Subscribe to our RSS','rby') ?></span>
						</a>
					</li>
				<?php } ?>
			</ul>
		<?php echo $after_widget; ?>
	<?php
	}
}
add_action('widgets_init',create_function('','return register_widget("rby_social");'));
