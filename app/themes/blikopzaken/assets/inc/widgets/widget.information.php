<?php
	
/**
 * @widget Contact info from TrendPress contact info
 */
class rby_contact extends WP_Widget {
	public function __construct()  {
		parent::__construct('rby_contact', __('Contact information','rby'), 'description='.__('Shows the specified contact information','rby'));
	}

	function form($instance) {
		$title = esc_attr($instance['title']);
	?>
 		<p>
 			<label for="<?php echo $this->get_field_id('title'); ?>">
 				<strong><?php _e('Title'); ?></strong><br />
 				<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
 			</label>
 		</p>
 		<p><?php printf(__('Change the contents of this widget on the <a href="%1$s">contact information</a> page.', 'rby'), admin_url('themes.php?page=rby-information')); ?></p>
	<?php
	}
	
	function update($new_instance,$old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
				
		return $instance;
	}
	
	function widget($args,$instance) {		
		$title = apply_filters('widget_title', $instance['title']);
		extract($args);
	?>
 		<?php echo $before_widget; ?>
 			<?php if ($title) { echo $before_title . $title . $after_title; } ?>	
			<div itemscope itemtype="http://schema.org/Organization">
				<p>
					<?php 
						if ($name = get_option('rby-company-name')) {
						echo '<span itemprop="name"><strong>'.$name.'</strong></span><br />';
					?>
					<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">	
						<?php
							} if ($address = get_option('rby-address')) { 
								echo '<span itemprop="streetAddress">'.$address.'</span><br />'; 
							} if ($postal_code = get_option('rby-postal-code')) {
								echo '<span itemprop="postalCode">'.$postal_code.'</span>';
							} if ($city = get_option('rby-city')) {
							 echo ' <span itemprop="addressLocality">'.$city.'</span><br />'; 
							} if ($country = get_option('rby-country')) {
							 echo '<span itemprop="addressCountry">'.$country.'</span>'; 
							}
						?>
					</span>
				</p>
				<p>
					<?php
						if ($email = get_option('rby-email')) { 
							echo'<span class="label">'.__('E-mail','rby').': </span><a itemprop="email" href="mailto:'.$email.'">'.$email.'</a><br />';
						} if ($telephone = get_option('rby-telephone')) { 
							echo '<span class="label">'.__('Telephone','rby').': </span><span itemprop="telephone">'.$telephone.'</span><br />';
						} if ($mobile = get_option('rby-mobile')) { 
							echo '<span class="label">'.__('Mobile','rby').': </span><span itemprop="mobile">'.$mobile.'</span><br />';
						} if ($fax = get_option('rby-fax')) { 
							echo '<span class="label">'.__('Fax','rby').': </span><span itemprop="faxNumber">'.$fax.'</span>';
						} 
					?>
				</p>
				<p>
					<?php
						if ($cc = get_option('rby-cc')) {
							echo '<span class="label">'.__('CC No','rby').': </span>'.$cc.'<br />';
						} if ($vat = get_option('rby-vat')) {
							echo '<span class="label" itemprop="vatID">'.__('VAT No','rby').': </span>'.$vat.'<br />';
						} if ($bankno = get_option('rby-bank-no')) {
							if ($bank = !get_option('rby-bank')) {
								$bank = "Bank";
							} else {
								$bank = get_option('rby-bank');
							}
							echo '<span class="label">'.$bank.': </span>'.$bankno;
						} 
					?>
				</p>
			</div>
		<?php echo $after_widget; ?>
	<?php
	}
}
add_action('widgets_init',create_function('','return register_widget("rby_contact");'));
