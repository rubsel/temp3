<?php
/**
 * @sidebars Register the sidebars
 */

function rby_register_sidebars() {
	new rbySidebar('blog',array(
		'name' => __('Blog','rby')
	));
	new rbySidebar('home',array(
		'name' => __('Home','rby')
	));
	new rbySidebar('footer',array(
		'name' => __('Footer','rby')
	));
	new rbySidebar('footer_2',array(
		'name' => __('Footer 2','rby')
	));
}
add_action('init','rby_register_sidebars');
?>