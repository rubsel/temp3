<?php
// Callback function to insert 'styleselect' into the $buttons array
function rby_mce_buttons( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
// Register our callback to the appropriate filter
add_filter( 'mce_buttons_2', 'rby_mce_buttons' );

/**
 * @misc Add editor styles
 */
function rby_add_editor_styles($settings) {
    $style_formats = array(
    	array(
    		'title' => __('Primary button','rby'),
    		'selector' => 'a',
    		'classes' => 'btn primary'
    	),
    	array(
    		'title' => __('Secondary button','rby'),
    		'selector' => 'a',
    		'classes' => 'btn secondary'
    	),
    	array(
    		'title' => __('More link','rby'),
    		'selector' => 'a',
    		'classes' => 'more'
    	)
    );
    $settings['style_formats'] = json_encode($style_formats);
    return $settings;
}
add_filter('tiny_mce_before_init','rby_add_editor_styles');

// Add Styling to WP Editor
add_editor_style( 'assets/css/custom-editor-style.css' );

/**
 * Convert a string to an URL (Add http:// if necessary)
 *
 * @param string $url
 */
function tp_maybe_add_http( $url ) {
	if( ! $url ) 
		return;
	
	if( ! strstr( $url, 'http://' ) && ! strstr( $url, 'https://' ) )
		$url = 'http://' . $url;
	
	return $url;
}

/**
 * @author Remove AIM, YIM, JABBER and add Facebook and LinkedIn
 */
function tp_modify_profile($media) {
	unset($media['aim']);
	unset($media['yim']);
	unset($media['jabber']);
	unset($media['googleplus']);

	$media['facebook'] = __('Facebook profile URL','rby');
	$media['linkedin'] = __('LinkedIn profile URL','rby');
	$media['googleplus'] = __('Google+ profile URL','rby');
	
	return $media;
}
add_filter('user_contactmethods','tp_modify_profile');

/**
 * @author Add checkbox to hide profile on the website
 */

function tp_hide_profile_edit($user) {
  $checked = (isset($user->hide_profile) && $user->hide_profile) ? ' checked="checked"' : '';
?>
	<label for="hide_profile">
		<input name="hide_profile" type="checkbox" id="hide_profile" value="1"<?php echo $checked; ?>>
		<?php _e('Hide my profile on the website','rby'); ?>
	</label>
<?php 
}
add_action('show_user_profile', 'tp_hide_profile_edit');
add_action('edit_user_profile', 'tp_hide_profile_edit');

function tp_hide_profile_update($user_id) {
	update_user_meta($user_id, 'hide_profile', isset($_POST['hide_profile']));
}
add_action('personal_options_update', 'tp_hide_profile_update');
add_action('edit_user_profile_update', 'tp_hide_profile_update');

/**
 * @Add odd/even to posts
 */
function oddeven_post_class ( $classes ) {
   global $current_class;
   $classes[] = $current_class;
   $current_class = ($current_class == 'odd') ? 'even' : 'odd';
   return $classes;
}
add_filter ( 'post_class' , 'oddeven_post_class' );
global $current_class;
$current_class = 'odd';

function rby_add_title_attachment_link($link, $id = null) {
    $id = intval( $id );
    $_post = get_post( $id );
    $post_title = esc_attr( $_post->post_excerpt );
    return str_replace('<a href', '<a title="'. $post_title .'" href', $link);
}
add_filter('wp_get_attachment_link', 'rby_add_title_attachment_link', 10, 2);

/**
 * @Add Fancii Excerpt
 */
 
function fancii_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'fancii_excerpt_length', 999 );

function fancii_excerpt ( $more ) {
	return '...';
}
add_filter( 'excerpt_more', 'fancii_excerpt' );

?>