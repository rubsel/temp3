��    b      ,  �   <      H     I     [  ^   c     �     �  	   �     �     �     �     	     	  V   	     k	     p	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
      
     4
     A
     V
     Z
     q
     �
  0   �
     �
     �
     �
     �
             
   $     /     <     C     Q     ]  	   t     ~     �     �  	   �  	   �     �     �     �  <   �  G         c     �     �     �  	   �     �     �     �     �     �  	   �                1     7     >     W  (   h     �  0   �  '   �                    2     E     Z  	   l     v  ?   �     �     �     �     �     �     �  }        �  �   �     *     -     =  f  O     �     �  j   �     ?     X     h     q     v     �  
   �     �  W   �     �               ,     A     I     \     c     h     u     �     �     �     �     �     �     �     �       7        O     e     |     �  $   �     �     �     �     �     �            	   *     4     A     N     U     d     q     x     �  2   �  G   �       "   /  	   R     \     e     r     �     �     �  	   �  	   �     �     �     �     �     �       "   $     G  .   g  +   �     �     �     �     �               0     9  T   H     �     �     �  
   �     �     �  �   �     �  �   �     3     8  
   H             L          M           +   ?      4   7             .                    :      `   E       K      -      "   Q   %       b   G   >       )         $   N   
            0       ;   C   1              S   9   a              H       8   !       	                     5   V   @          T                  Y   A   O       \      =   R   D   U   [   <              6   2      F   W          ]   *   ^              X       ,   (   &      /   #   3   _       B   I   '          J   Z   P    Add us on Google+ Address Advanced Custom Fields is not active. Download and activate it for the best use of this theme. Back to the overview Bank No. Bank name Blog Button link Button text CC No CC No. Change the contents of this widget on the <a href="%1$s">contact information</a> page. City Comments are closed Company name Connect with us on LinkedIn Contact Contact information Content Country Description Design & development E-mail E-mail newsletter Editable title, tekst and button Facebook URL Facebook profile URL Fax Follow us on Instagram Follow us on Twitter Footer Found %2$s articles containing the keyword: %1$s General settings Get in touch Google Plus URL Google+ profile URL Hide my profile on the website Home Icon types Image Widget Image: Instagram URL Large icons Like our Facebook page Link type LinkedIn URL LinkedIn profile URL Mobile More link New title News Newsletter / mailto link Next Nothing could be found at this location. Maybe try a search? One response to &ldquo;%2$s&rdquo; %1$s responses to &ldquo;%2$s&rdquo; Oops! Something went wrong here. Open this link in new window Pages: Postal code Posted on Posts about Posts by Previous Primary button Primarymenu Read more Read more link Registration numbers & financial Reply Search Search Results for: %1$s Secondary button Show RSS feed in the social media widget Show button / read more link Shows links to specified social network profiles Shows the specified contact information Sitename Small icons Small icons with text Social media links Subscribe to our RSS Subscribe via RSS Telephone Text with button This is an image widget created with Advanced Custom Fields PRO Title Title: Twitter URL VAT No VAT No. View our YouTube channel You are using a very old version of Internet Explorer. For the best experience please upgrade (for free) to a modern browser: YouTube URL Your search for <em>&quot;%1$s&quot;</em> did not match any documents. Please make sure all your words are spelled correctly or try different keywords. by in the category labelSearch for: Project-Id-Version: rby v1.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-04-04 14:26+0200
PO-Revision-Date: 2017-04-04 14:29+0200
Last-Translator: Ruben Zwiers <ruben@rubsel.com>
Language-Team: 
Language: en_EN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 2.0
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: ..
 Voeg ons toe op Google+ Adres De plugin Advanced Custom Fields is niet actief. Download en activeer deze voor het beste beste resultaat. Terug naar het overzicht Rekeningnummer. Banknaam Blog Button link Button tekst KVK nummer KVK nummer. Pas de inhoud van deze widget aan via de <a href="%1$s">contact information</a> pagina. Stad Reageren niet meer mogelijk Bedrijfsnaam Verbind via LinkedIn Contact Contact informatie Inhoud Land Omschrijving Ontwerp & ontwikkeling E-mail E-mail nieuwsbrief Aanpasbare titel, tekst en knop Facebook URL Facebook URL Fax Volg ons op Instagram Volg ons op Twitter Footer 1 Er zijn %2$s berichten gevonden met het zoekwoord: %1$s Algemene instellingen Neem direct contact op Google Plus URL Google+ URL Verberg mijn profiel op de website.  Home  Icoon types Widget met een afbeelding Afbeelding: Instagram URL Grote iconen Like onze Facebook pagina Link type LinkedIn URL LinkedIn URL Mobiel Lees meer link Nieuwe titel Nieuws Newsletter / mailto link Volgende Er is hier niets gevonden. Probeer eens te zoeken: Één reactie op &ldquo;%2$s&rdquo; %1$s reacties op &ldquo;%2$s&rdquo; Oeps! Hier ging iets mis. Open deze link in een nieuw scherm Pagina's: Postcode Geplaatst op Berichten over Berichten door Vorige Primaire button Hoofdmenu Lees meer Lees meer link Registraties & financieel Beantwoorden Zoeken Zoekresultaten voor: %1$s Secundaire button Laat de RSS feed in de widget zien Laat knop / lees meer link zien Laat de ingevulde sociale media profielen zien Laat de ingevulde contact informatie zien.  Sitenaam Kleine iconen Kleine iconen met tekst Sociale media links Abonneer op onze RSS-feed Abonneer via RSS Telefoon Tekst met knop Dit is een widget met een afbeelding. Gegenereerd vanuit Advanced Custom Fields PRO. Titel Titel: Twitter URL BTW nummer BTW nummer. Bekijk ons YouTube kanaal Je maakt gebruik van een oude versie van Internet Explorer. Voor de beste ervaring op deze website raden we je aan (gratis) een modernere browser te gebruiken. YouTube URL Het zoeken naar <em>&quot;%1$s&quot;</em> heft geen resultaat opgeleverd. Zorg dat je zoekwoorden goed gespeld zijn of probeer het nog eens met andere termen.  door in de categorie Zoek naar: 