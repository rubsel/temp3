// @codekit-prepend 'assets/js/jquery.fancybox.js'
// @codekit-prepend 'assets/js/jquery.flexslider.js'

jQuery(document).ready(function($){

// Theme Main JQuery functions
	$('.handle').on('click', function() {
		$('nav ul.main-nav').toggleClass('show');
	});

// Adding :hover & :focus states on Mobile
	window.onload = function() {
	  if(/iP(hone|ad)/.test(window.navigator.userAgent)) {
	    document.body.addEventListener('touchstart', function() {}, false);
	  }
	};


// Initialize the Lightbox and add rel="gallery" to all gallery images when the gallery is set up using [gallery link="file"] so that a Lightbox Gallery exists
	$(".fancybox").fancybox();
	$(".gallery a[href$='.jpg'], .gallery a[href$='.png'], .gallery a[href$='.jpeg'], .gallery a[href$='.gif']").attr('rel','gallery').fancybox();
	$('a[href$="jpg"], a[href$="png"], a[href$="jpeg"]').fancybox( {
	    openEffect: 'elastic',
	    closeEffect: 'elastic',
	    prevEffect : 'elastic',
	    nextEffect : 'elastic',
	   	scrolling: 'no',
	   	autoDimensions : 'false',
	   	loop: 'false',
	    padding: 0,
	    margin: 25,
	});


// Slidehshow homeheader
	$(document).ready(function() {
		$('.flexslider').flexslider({
		   animation: "fade",
		});
	});	
    

});

