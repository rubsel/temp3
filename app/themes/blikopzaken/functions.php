<?php

/**
 * Make theme available for translation
 * use rby to trigger the tranbslation
*/
load_theme_textdomain( 'rby', get_template_directory() . '/assets/lang' );


/**
 *	Move wp-core jQuery to footer
 */
function rby_jquery_into_footer() {

    if( is_admin() ) {
        return;
    }

    $wp_scripts = wp_scripts();

    $wp_scripts->add_data( 'jquery',         'group', 1 );
    $wp_scripts->add_data( 'jquery-core',    'group', 1 );
    $wp_scripts->add_data( 'jquery-migrate', 'group', 1 );
}

add_action( 'wp_enqueue_scripts', 'rby_jquery_into_footer', 0 );

/**
 *	Enqueue styles & scripts
 */
function rby_scripts() {
	// load main styling 
		wp_enqueue_style( 'rby-styling', get_template_directory_uri() . '/assets/css/style.compiled.css' );
	// load main JS file
		wp_enqueue_script( 'rby-js', get_template_directory_uri() . '/assets/js/min.all.js' , array('jquery'), null, true);
	}
add_action( 'wp_enqueue_scripts', 'rby_scripts' );

/**
 *  Enqueue Google Fonts
 */
function rby_load_fonts() {
        wp_register_style( 'googleFonts', '//fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i' );
        wp_enqueue_style( 'googleFonts' );
    }
add_action('get_footer', 'rby_load_fonts');


function rby_add_footer_styles() {
        wp_register_style( 'ionicons', '//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' );
		wp_enqueue_style( 'ionicons' );
	}
add_action( 'get_footer', 'rby_add_footer_styles' );

/**
 * This theme uses wp_nav_menu() in one location.
*/
register_nav_menus( array(
	'primary'  => __( 'Primarymenu', 'rby' ),
) );


add_theme_support( 'post-thumbnails', array( 'post' ));
add_image_size( 'header', 1920, 600, true );
add_image_size( 'featured', 500, 600, true );


/**
 * Include php files
 */
include('assets/inc/functions.misc.php');
include('assets/inc/class.rbySidebar.php');
include('assets/inc/functions.sidebars.php');
include('assets/inc/functions.information.php');
//include('assets/inc/functions.posttypes.php');

/**
 * Include widget php files
 */
 include('assets/inc/widgets/widget.information.php');
 include('assets/inc/widgets/widget.text.button.php');
 include('assets/inc/widgets/widget.social.php');

/*
 * Check if ACF is activated or show an admin error
 */
if( class_exists('acf') ) { 

    include('assets/inc/widgets/widget.image.php');
    include('assets/inc/functions.acf.php');

} else {

    function sample_admin_notice__error() {
        $class = 'notice notice-error';
        $message = __( 'Advanced Custom Fields is not active. Download and activate it for the best use of this theme.', 'rby' );

        printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
    }
    add_action( 'admin_notices', 'sample_admin_notice__error' );
}

add_filter( 'frm_load_dropzone', '__return_false' );


