

<?php if( is_page('contact') ) { ?>

<?php }  else { ?>
<footer id="footer" class="site-footer">
	<div class="container">
		<div class="row">
			<div class="site-footer-inner sidebar">
				<div class="col-md-3 col-md-offset-3">
					<?php if ( function_exists('dynamic_sidebar') ) dynamic_sidebar('footer'); ?>
				</div>
				<div class="col-md-3">
					<?php if ( function_exists('dynamic_sidebar') ) dynamic_sidebar('footer_2'); ?>
				</div>
			</div>
		</div>
	</div> <!-- close .container -->
</footer> <!-- close #footer -->
<?php } ?>

<footer id="colophon">
	<div class="container">
		<div class="row">
			<div class="site-footer-inner">

			<div class="site-info col-md-4">
				<p>&copy; <?php bloginfo('name'); ?>, <?php echo date('Y'); ?></p>
			</div>

			<div id="social" class="col-md-4">
				<ul class="social <?php echo $type; ?> clearfix">
					<?php if($twitter = get_option('rby-twitter')) { ?>
						<li class="twitter">
							<a rel="external" href="<?php echo $twitter; ?>" title="<?php _e('Follow us on Twitter','rby') ?>" target="_blank">
								<i class="ion-social-twitter"></i>
							</a>
						</li>
					<?php } if($facebook = get_option('rby-facebook')) { ?>
						<li class="facebook">
							<a rel="external" href="<?php echo $facebook; ?>" title="<?php _e('Like our Facebook page','rby') ?>" target="_blank">
								<i class="ion-social-facebook"></i>
							</a>
						</li>
					<?php } if($instagram = get_option('rby-instagram')) { ?>
						<li class="instagram">
							<a rel="external" href="<?php echo $instagram; ?>" title="<?php _e('Follow us on Instagram','rby') ?>" target="_blank">
								<i class="ion-social-instagram"></i>
							</a>
						</li>
					<?php } if($linkedin = get_option('rby-linkedin')) { ?>
						<li class="linkedin">
							<a rel="external" href="<?php echo $linkedin; ?>" title="<?php _e('Connect with us on LinkedIn','rby') ?>" target="_blank">
								<i class="ion-social-linkedin"></i>
							</a>
						</li>
						<?php } if($googleplus = get_option('rby-googleplus')) { ?>
						<li class="googleplus">
							<a rel="external" href="<?php echo $googleplus; ?>" title="<?php _e('Add us on Google+','rby') ?>" target="_blank">
								<i class="ion-social-googleplus"></i>
							</a>
						</li>
					<?php } if($youtube = get_option('rby-youtube')) { ?>
						<li class="youtube">
							<a rel="external" href="<?php echo $youtube; ?>" title="<?php _e('View our YouTube channel','rby') ?>" target="_blank">
								<i class="ion-social-youtube"></i>
							</a>
						</li>
					<?php } ?>
				</ul>
			</div>

			<div class="credits" class="col-md-4">
				<p><?php _e('Design & development','rby');?> <a href="//blikopzaken.nl" target="_blank">Blik op Zaken</a></p>
			</div>
		</div>
		</div><!-- close .row -->
	</div><!-- close .container -->
</footer><!-- close #colophon -->


<?php wp_footer(); ?>

	<!--[if IE 6]>
		<div class="ie6">
			<p><?php _e('You are using a very old version of Internet Explorer. For the best experience please upgrade (for free) to a modern browser:','rby'); ?>
			<ul>
				<li><a href="http://www.mozilla.com/" rel="nofollow external">Firefox</a></li>
				<li><a href="http://www.google.com/chrome/" rel="nofollow external">Google Chrome</a></li>
				<li><a href="http://www.apple.com/safari/" rel="nofollow external">Safari</a></li>
				<li><a href="http://www.microsoft.com/windows/internet-explorer/" rel="nofollow external">Internet Explorer</a></li>
			</ul>
		</div>
	<![endif]-->


</body>
</html>