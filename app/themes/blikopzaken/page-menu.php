<?php get_header(); ?>
<?php $image = wp_get_attachment_image_src(get_field('headerfoto'), 'header'); ?>
<div class="header" style="background: url(<?php echo $image[0]; ?>);">
	<!-- header afbeelding -->
</div>
<div class="main-content">
	<div class="container">
		<div class="row">
			<div id="content" class="main-content-inner col-sm-12 col-md-10 col-md-offset-1">
		
			<?php while ( have_posts() ) : the_post(); ?>
				
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
				<div class="row">
					<div class="entry-content col-md-8">
						<h1><?php the_title(); ?></h1>
						<?php the_content(); ?>
					</div><!-- .entry-content -->

					<figure class="col-md-4">
						<?php the_post_thumbnail('featured');?>
					</figure>
                
					
					<div class="menukaart col-xs-12">
                       
                        <?php $s = 1; ?>
                        <center><h2 id="specials">Specials</h2></center>
					    <hr>
                        <p><?php the_field('specials_beschrijving'); ?></p>

                        <table class="specials">

					    <?php if(have_rows('specials')) : while(have_rows('specials')) : the_row(); ?>

					       <tr>
                                <td>
                                    S<?php echo $s; ?>
                                </td>
					            <td>
					                <b><?php the_sub_field('naam'); ?></b>
                                </td>
                                <td>
					                €<?php the_sub_field('prijs'); ?>
                                </td>
					        </tr>
					        <tr>
					            <td colspan="3">
					                <?php the_sub_field('beschrijving'); ?>
					            </td>
					        </tr>
					       
					    
					    <?php $s++; endwhile; endif; ?>
					    </table>
                        
					    
					    <center>
					    
					        <h2><?php if ($telephone = get_option('rby-telephone')): ?>
				<?php _e('Get in touch','rby');?>: <nobr><?php echo $telephone; ?></nobr><?php endif; ?> </h2>
					    
					        <a href="<?php the_field('bestelknop');?>" class="btn primary">Of bestel schriftelijk</a>
					        
					    </center>
					    <hr>
					

                        <br>
                        
                        <?php $b = 1; ?>
                        <center><h2 id="broodjes">Broodjes</h2></center>
					    <hr>
                        <center><h3>Regulier beleg</h3></center>
					    <p><?php the_field('broodjes_beschrijving'); ?></p>
					    <table class="broodjes">
					   
					    
					    <?php if(have_rows('broodjes')) : while(have_rows('broodjes')) : the_row(); ?>
					        <tr>
                                <td>
                                    B<?php echo $b; ?>
                                </td>
					            <td>
					                <b><?php the_sub_field('naam'); ?></b>
                                </td>
                                <td>
					                €<?php the_sub_field('prijs'); ?>
                                </td>
					        </tr>
					    
					    <?php $b++; endwhile; endif; ?>
					    
					    </table>
                        
                        <hr>
                        <center><h3>Luxe beleg</h3></center>
					    <p><?php the_field('broodjes_beschrijving_2'); ?></p>
					    <table class="broodjes">
					   
					    
					    <?php if(have_rows('broodjes_2')) : while(have_rows('broodjes_2')) : the_row(); ?>
					        <tr>
                                <td>
                                    B<?php echo $b; ?>
                                </td>
					            <td>
					                <b><?php the_sub_field('naam'); ?></b>
                                </td>
                                <td>
					                €<?php the_sub_field('prijs'); ?>
                                </td>
					        </tr>
					    
					    <?php $b++; endwhile; endif; ?>
					    
					    </table>
                        
                        <hr>
                        <center><h3>Speciale broodjes</h3></center>
					    <p><?php the_field('broodjes_beschrijving_3'); ?></p>
					    <table class="broodjes">
					   
					    
					    <?php if(have_rows('broodjes_3')) : while(have_rows('broodjes_3')) : the_row(); ?>
					        <tr>
                                <td>
                                    B<?php echo $b; ?>
                                </td>
					            <td>
					                <b><?php the_sub_field('naam'); ?></b><br>
                                    <?php the_sub_field('beschrijving'); ?>
                                </td>
                                
                                <td>
					                €<?php the_sub_field('prijs'); ?>
                                </td>
					        </tr>
					    
					    <?php $b++; endwhile; endif; ?>
					    
					    </table>
                        
                        
                        
					    <center>
					    
					        <h2><?php if ($telephone = get_option('rby-telephone')): ?>
				<?php _e('Get in touch','rby');?>: <nobr><?php echo $telephone; ?></nobr><?php endif; ?> </h2>
					    
					        <a href="<?php the_field('bestelknop');?>" class="btn primary">Of bestel schriftelijk</a>
					        
					    </center>
					  <hr>  
                      <br>
                       
                       
                        <?php $d = 1; ?>
                        <center><h2 id="dranken">Dranken</h2></center>
                        <hr>
                        <p><?php the_field('dranken_beschrijving'); ?></p>
					    <table class="dranken">
                        
                            <?php if(have_rows('dranken')) : while(have_rows('dranken')) : the_row(); ?>
					        <tr>
                                <td>
                                    D<?php echo $d; ?>
                                </td>
					            <td>
					                <b><?php the_sub_field('naam'); ?></b>
                                </td>
                                <td>
					                €<?php the_sub_field('prijs'); ?>
                                </td>
					        </tr>
					    
					        <?php $d++; endwhile; endif; ?>
					        
					    </table>
              <center>
					    
					        <h2><?php if ($telephone = get_option('rby-telephone')): ?>
				<?php _e('Get in touch','rby');?>: <nobr><?php echo $telephone; ?></nobr><?php endif; ?> </h2>
					    
					        <a href="<?php the_field('bestelknop');?>" class="btn primary">Of bestel schriftelijk</a>
					        
					    </center>
               <hr>
               </div>
                </div>
				</article><!-- #post-## -->
				
			<?php endwhile; // end of the loop. ?>
		
			</div><!-- close .main-content-inner -->
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->
<?php get_footer(); ?>
