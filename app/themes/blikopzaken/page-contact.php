<?php get_header(); ?>
<?php $image = wp_get_attachment_image_src(get_field('headerfoto'), 'header'); ?>
<div class="header" style="background: url(<?php echo $image[0]; ?>);">
	<!-- header afbeelding -->
</div>
<div class="main-content">
	<div class="container">
		<div class="row">
			<div id="content" class="main-content-inner col-sm-12 col-md-10 col-md-offset-1">
		
			<?php while ( have_posts() ) : the_post(); ?>
				
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
					<div class="entry-content col-md-8">
						<h1><?php the_title(); ?></h1>
						<?php the_content(); ?>
					</div><!-- .entry-content -->

					<figure class="col-md-4">
						<?php the_post_thumbnail('featured');?>
						<br /><br />
						<p class="address">
							<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
							<strong><?php bloginfo("name" );?></strong>	<br />
							<?php
								if ($address = get_option('rby-address')) { 
									echo '<span itemprop="streetAddress">'.$address.'</span><br />'; 
								} if ($postal_code = get_option('rby-postal-code')) {
									echo '<span itemprop="postalCode">'.$postal_code.'</span>';
								} if ($city = get_option('rby-city')) {
								 echo ' <span itemprop="addressLocality">'.$city.'</span><br />'; 
								} if ($country = get_option('rby-country')) {
								 echo '<span itemprop="addressCountry">'.$country.'</span>'; 
								} if ($telephone = get_option('rby-telephone')) {
								 echo '<span">'.$telephone.'</span>'; 
								} 
							?>
						</span>
						</p>
					</figure>
				</article><!-- #post-## -->
				
			<?php endwhile; // end of the loop. ?>
		
			</div><!-- close .main-content-inner -->
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->
<?php get_footer(); ?>
