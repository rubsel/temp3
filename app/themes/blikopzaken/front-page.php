<?php get_header(); ?>
<!--
<div id="frontslide" class="flexslider">

    <ul class="slides">

    	<?php $images = get_field('slider');
			if( $images ): ?>
        <?php foreach( $images as $image ): ?>
            <li style="background-image: url(<?php echo $image['url']; ?>);">
            </li>
        <?php endforeach; ?>

		<?php endif; ?>
    </ul>

    


</div>
-->
<?php $image = wp_get_attachment_image_src(get_field('actie_foto'), 'header'); ?>
<div class="home-header" style="background: url(<?php echo $image[0]; ?>);">
	<!-- header afbeelding -->
	
	<div class="container">
	    
	    <div class="row">
	        <div class="header_content">
	            <div class="actie-titel">
	                <?php the_field('actie_naam'); ?>
	            </div>
	            <div class="actie-tekst">
                
                    <div class="tekst">
	                <?php the_field('actie_tekst'); ?>
	                </div>
	                
	                <div class="button">
                        <a href="<?php the_field('actie_link'); ?>"><?php the_field('knop_tekst');?></a>
	                </div>
	                
	            </div>
	            
	   
	        </div>
	    </div>
	</div>
</div>

<div class="services">
	<div class="container">
		<div class="row">
			<div class="services-circle col-md-12">
			     
			     <?php if(have_rows('diensten')) : while(have_rows('diensten')) : the_row(); ?>
                        
                        
                        <div class="col-md-3 col-sm-6">
                        <?php $serviceImg = wp_get_attachment_image_src(get_sub_field('foto'), 'medium'); ?>
                         <div class="service" style="background:url(<?php echo $serviceImg[0]; ?>);">
                             
                             <div class="service-contents-wrapper">
                             
                             <div class="service-contents">
			                     <h3 class="title"><?php the_sub_field('titel'); ?></h3>
			                     <p><?php the_sub_field('beschrijving'); ?></p>
                           </div>
                            </div>
                            
                             <a class="overlay-link" href="<?php the_sub_field('link');?>"></a>
                             
			             </div>
                        </div>



			     <?php endwhile; endif; ?>
			</div>
		</div>
	</div>
</div>

<div class="main-content">
	<div class="container">
		<div class="row">
			<div id="content" class="main-content-inner col-md-8 col-md-offset-2">

				<?php while ( have_posts() ) : the_post(); ?>
					
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<h1><?php the_title(); ?></h1>
					
						<div class="entry-content">
							<?php the_content(); ?>
							<?php
								wp_link_pages( array(
									'before' => '<div class="page-links">' . __( 'Pages:', 'rby' ),
									'after'  => '</div>',
								) );
							?>
						</div><!-- .entry-content -->
					</article><!-- #post-## -->

				<?php endwhile; // end of the loop. ?>
				
			</div><!-- close .main-content-inner -->
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->

<?php get_footer(); ?>
