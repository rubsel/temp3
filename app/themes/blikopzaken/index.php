<?php get_header(); ?>

<?php $image = wp_get_attachment_image_src(get_field('headerfoto', get_option('page_for_posts')), 'header'); ?>
<div class="header" style="background: url(<?php echo $image[0]; ?>);">
	<!-- header afbeelding -->
</div>

<div class="main-content">
	<div class="container">
		<div class="row">
			<div id="content" class="main-content-inner col-md-10 col-sm-12 col-md-offset-1">
		
			<h1>
				<?php 
					if( is_category() || is_tag() || is_tax() )
						single_term_title( __( 'Posts about', 'rby' ) . ' ' );
					elseif( is_author() )
						echo __( 'Posts by', 'rby' ) . ' ' . get_the_author_meta( 'display_name' );
					else
						_e( 'News', 'rby' );
				?>
			</h1>
			
			<?php if( have_posts() ) : ?>

				<?php while( have_posts() ) : the_post(); ?>
			
					<article <?php post_class('clearfix'); ?>>
					
						<h2>
							<a href="<?php the_permalink(); ?>">
								<?php the_title(); ?>
							</a>
						</h2>
						
						<p class="meta">
							<?php _e( 'Posted on', 'rby' ); ?> <time datetime="<?php the_time( 'Y-m-d' ); ?>"><?php echo get_the_date(); ?></time>
							<?php _e( 'by', 'rby' ) ?> <?php the_author_posts_link(); ?> 
							<?php _e( 'in the category', 'rby' ) ?> <?php the_category( ', ' ) ?>
						</p>
						
						<figure class="thumb">
							<a href="<?php the_permalink(); ?>">
								<?php the_post_thumbnail( 'thumbnail' ); ?>
							</a>
						</figure>
						
						<?php the_excerpt(); ?>
						
						<a class="more" href="<?php the_permalink();?>"> <?php _e('Read more','rby');?> </a>
						<hr >
					</article>

		
				<?php endwhile; ?>
		

		
			<?php endif; ?>
		
			<?php the_posts_pagination( array(
				'mid_size' => 2,
			    'screen_reader_text' => ' ', 
			    'prev_text'          => __( 'Previous', 'rby' ),
			    'next_text'          => __( 'Next', 'rby' ),
			    'before_page_number' => '',
			) );?>
			
			</div><!-- close .main-content-inner -->
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->

<?php get_footer(); ?>