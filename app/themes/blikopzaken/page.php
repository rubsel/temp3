<?php get_header(); ?>
<?php $image = wp_get_attachment_image_src(get_field('headerfoto'), 'header'); ?>
<div class="header" style="background: url(<?php echo $image[0]; ?>);">
	<!-- header afbeelding -->
</div>
<div class="main-content">
	<div class="container">
		<div class="row">
			<div id="content" class="main-content-inner col-sm-12 col-md-10 col-md-offset-1">
		
			<?php while ( have_posts() ) : the_post(); ?>
				
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				    
				    <?php if(has_post_thumbnail()){ ?>
					<div class="entry-content col-md-8">
						<h1><?php the_title(); ?></h1>
						<?php the_content(); ?>
					</div><!-- .entry-content -->

					<figure class="col-md-4">
						<?php the_post_thumbnail('featured');?>
					</figure>
					
					<?php } else { ?>
					<div class="entry-content col-md-10 col-md-offset-1">
						<h1><?php the_title(); ?></h1>
						<?php the_content(); ?>
					</div><!-- .entry-content -->
					<?php } ?>
					
				</article><!-- #post-## -->
				
			<?php endwhile; // end of the loop. ?>
		
			</div><!-- close .main-content-inner -->
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->
<?php get_footer(); ?>
