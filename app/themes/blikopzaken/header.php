<!DOCTYPE html>
<!--[if IE 7 ]><html class="no-js ie ie7" lang="nl"><![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8" lang="nl"><![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9" lang="nl"><![endif]-->
<!--[if gt IE 9]><html class="no-js ie" lang="nl"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js" <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php wp_title( '|', true, 'right' ); ?> <?php bloginfo('name');?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
	<?php do_action( 'before' ); ?>

<header id="masthead" class="site-header">
	<div class="container">
		<div class="row">
			<div class="site-header-inner">
				<div class="site-branding col-md-3">
					<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				</div>

				<div class="head_nav col-md-9">				
					<div class="contact">
						<?php if ($mobile = get_option('rby-mobile')): ?> 
							<?php _e('Get in touch','rby');?>: 	<?php echo $mobile; ?>  
						<?php elseif ($telephone = get_option('rby-telephone')): ?>
							<?php _e('Get in touch','rby');?>:<br><b><?php echo $telephone; ?> </b>
						<?php else: ?>
							<!-- do or do not, there is no try -->
						<?php endif;?>
					</div>
				<nav class="navbar navbar-default">
				<div class="handle"> 
					<div class="mobile-icon">
						<div class="line"></div>
						<div class="line"></div>
						<div class="line"></div>
					</div>
				</div>
				<!-- The WordPress Menu goes here -->
					<?php wp_nav_menu(
						array(
							'theme_location' => 'primary',
							'menu_class' => 'main-nav',
							'fallback_cb' => 'true',
							'menu_id' => 'main-nav',
							'depth' => '0'
						)
					); ?>
				</nav><!-- .navbar -->

                </div>
			</div>
		</div> 
	</div><!-- .container -->
</header><!-- #masthead -->

