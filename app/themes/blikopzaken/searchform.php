<?php ?>
<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
		<input type="search" class="search-field" placeholder="<?php _e('Search','rby'); ?>&hellip;" value="<?php echo esc_attr( get_search_query() ); ?>" name="s" title="<?php _ex( 'Search for:', 'label', 'rby' ); ?>">
	</label>
	<button type="submit" class="search-submit">
	<span class="search-text"><?php _e('Search','rby'); ?></span>
	</button>
</form>
